﻿using System;
using TeletracLibraryKH;

namespace TeletracNavmanConsoleKH
{
    /// <summary>
    ///  Console test application.
    /// </summary>
    class Program
    {
        /// <summary>
        ///  Console entry.
        /// </summary>
        /// <param name="args">string[</param>
        static void Main(string[] args)
        {
            try
            {
                DataAccess objData = new DataAccess();
                VehicleActivity vehicleActivity = objData.ReadXML<VehicleActivity>();

                string latitude = vehicleActivity.Latitude.ToString();
                string longitude = vehicleActivity.Longitude.ToString();
                string json = objData.JSON(vehicleActivity);
            }
            catch(Exception ex)
            {
                string error = ex.Message;
            }
        }
    }
}