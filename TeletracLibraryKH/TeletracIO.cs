﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace TeletracLibraryKH
{
    /// <summary>
    ///  One instance of Serializer to reduce risk of memory leaks.
    /// </summary>
    public class SingletonSerializer
    {
        // One instance of XML sourced object.
        public static XmlSerializer Instance { get; } = new XmlSerializer(typeof(VehicleActivity));
    }

    /// <summary>
    ///  File IO.
    /// </summary>
    public class DataAccess
    {
        // Exception.
        protected TeletracException _exception = null;

        public TeletracException DataAccessException => _exception;

        /// <summary>
        ///  Access XML file for processing on site.
        ///  Throws Exception : 0X80000000
        /// </summary>
        public T ReadXML<T>()
        {
            // One instance of singleton serializer to avoid memory leaks.
            XmlSerializer serializer = SingletonSerializer.Instance;
            object objT = null;
            try
            {
                string path = Directory.GetCurrentDirectory();
                System.IO.DirectoryInfo directoryInfo =
                    System.IO.Directory.GetParent(path);

                string targetFile = directoryInfo.FullName + @"\TeletracLibraryKH\CoreXMLFile.xml";

                using (FileStream fileStream = new FileStream(targetFile, FileMode.Open))
                {
                    var result = serializer.Deserialize(fileStream);

                    if (result == null)
                    {
                        throw new Exception("XML data not obtained.");
                    }

                    objT = (T)result;
                }

            }
            catch(Exception ex)
            {
                _exception = new TeletracException("XML Error", ex, TeletracException.E_READ_XML);
                throw _exception;
            }

            return (T)objT;
        }

        /// <summary>
        ///  JSON serializeable object.
        ///  Throws Exception : 0X80000001
        /// </summary>
        /// <typeparam name="T">Type Generic</typeparam>
        /// <param name="serializeObject">Object to serialize</param>
        /// <returns>string</returns>
        public string JSON<T>(T serializeObject)
        {
            string result = string.Empty;
            try
            {
                result = Newtonsoft.Json.JsonConvert.SerializeObject(serializeObject);
            }
            catch (Exception ex)
            {
                _exception = new TeletracException("XML Error", ex, TeletracException.E_WRITE_JSON);
                throw _exception;
            }
            return result;
        }
    }
}