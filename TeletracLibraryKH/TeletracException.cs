﻿using System;

namespace TeletracLibraryKH
{
    /// <summary>
    ///  Exceptions for this application.
    /// </summary>
    public class TeletracException : Exception
    {
        // Errpr cpdes.
        public const int E_READ_XML = unchecked((int)0X80000000);
        public const int E_WRITE_JSON = unchecked((int)0X80000001);

        int _error = 0;

        /// <summary>
        ///  Teletrac exception.
        /// </summary>
        /// <param name="message">String message</param>
        /// <param name="innerException">Exception</param>
        /// <param name="error">Error code</param>
        public TeletracException(string message, Exception innerException, int error) : 
            base( message,  innerException)
        {
            _error = error;

            LogError(this.Message, this.InnerException, error);
        }

        /// <summary>
        ///  TODO: Use any existing logging methods to store when errors occurred and their details.
        /// </summary>
        /// <param name="message">String message</param>
        /// <param name="innerException">Exception</param>
        /// <param name="error">Error code</param>
        private void LogError(string message, Exception innerException, int error)
        {
            // User logging or 3rd party logging goes here.

            // Log error.................
        }

        /// <summary>
        ///  Error code in HEX.
        /// </summary>
        /// <param name="ex"><Exception/param>
        /// <returns>string</returns>
        public static string GetErrorCode(Exception ex)
        {
            int result = 0;
            if (ex is TeletracException)
            {
                TeletracException exTele = (TeletracException)ex;
                result = exTele._error;
            }

            return result.ToString("X");
        }
    }
}