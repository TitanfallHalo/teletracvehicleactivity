﻿/* 
    Licensed under the Apache License, Version 2.0
    
    http://www.apache.org/licenses/LICENSE-2.0

 * */
using System.Xml.Serialization;


namespace TeletracLibraryKH
{
    [XmlRoot(ElementName = "VehicleActivity")]
    public class VehicleActivity
    {
        [XmlElement(ElementName = "ActivityDateTime")]
        public string ActivityDateTime { get; set; }
        [XmlElement(ElementName = "Latitude")]
        public string Latitude { get; set; }
        [XmlElement(ElementName = "Longitude")]
        public string Longitude { get; set; }
        [XmlElement(ElementName = "GPSValid")]
        public string GPSValid { get; set; }
        [XmlElement(ElementName = "Location")]
        public string Location { get; set; }
        [XmlElement(ElementName = "IgnitionOn")]
        public string IgnitionOn { get; set; }
        [XmlElement(ElementName = "HDG")]
        public string HDG { get; set; }
        [XmlElement(ElementName = "Speed")]
        public string Speed { get; set; }
        [XmlElement(ElementName = "EventSubType")]
        public string EventSubType { get; set; }
        [XmlElement(ElementName = "ODOMeter")]
        public string ODOMeter { get; set; }
        [XmlElement(ElementName = "VehicleID")]
        public string VehicleID { get; set; }

    }
}