﻿namespace TeletracNavmanSiteKH.Models
{
    /// <summary>
    ///  MVC Model for Vehicle Activity.
    /// </summary>
    public class PositionModel
    {
        public string DateAndTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Speed { get; set; }
        public string Heading { get; set; }
    }
}
