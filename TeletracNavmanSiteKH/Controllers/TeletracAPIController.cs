﻿using Microsoft.AspNetCore.Mvc;
using System;
using TeletracLibraryKH;

namespace TeletracNavmanSiteKH.Controllers
{
    /// <summary>
    ///  Rest API.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TeletracAPIController : ControllerBase
    {
        // GET: api/TeletracAPI
        [HttpGet]
        public IActionResult Get()
        {
            // Return vehicle activity as JSON.
            DataAccess objData = new DataAccess();
            string result = string.Empty;
            try
            {
                VehicleActivity vehicleActivity = objData.ReadXML<VehicleActivity>();
                result = objData.JSON(vehicleActivity);
            }
            catch(Exception ex)
            {
                return NotFound(TeletracException.GetErrorCode(ex) + " : " + ex.Message);
            }

            return Ok(result);
        }
    }
}