﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TeletracLibraryKH;
using TeletracNavmanSiteKH.Models;

namespace TeletracNavmanSiteKH.Controllers
{
    /// <summary>
    ///  Home controller. Home/
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        ///  Home. Requirement to redirect to Teletrac page implemented.
        /// </summary>
        /// <returns>IActionResult</returns>
        public IActionResult Index()
        {
            return Redirect("Home/Teletrac");
            //return View(); (if not redirecting)
        }

        /// <summary>
        ///  Cache XML data.
        /// </summary>
        /// <returns>IActionResult</returns>
        public IActionResult Teletrac()
        {
            try
            {
                DataAccess objData = new DataAccess();
                VehicleActivity vehicleActivity = objData.ReadXML<VehicleActivity>();

                string latitude = vehicleActivity.Latitude.ToString();
                string longitude = vehicleActivity.Longitude.ToString();
                string json = objData.JSON(vehicleActivity);

                // Randomize co-ordinates.
                Random random = new Random();
                int heading = random.Next(0, 359);

                vehicleActivity.HDG = heading.ToString() + "°";
                ViewBag.StyleDiv = "display:none";
                ViewBag.BtnDiv = "";
                TempData["VehicleActivity"] = JsonConvert.SerializeObject(vehicleActivity);

            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return View();

        }

        /// <summary>
        ///  Request to show data.
        /// </summary>
        /// <param name="s">string</param>
        /// <returns>IActionResult</returns>
        [HttpPost]
        public IActionResult Teletrac(string s)
        {
            VehicleActivity vehicleActivity = JsonConvert.DeserializeObject<VehicleActivity>(TempData["VehicleActivity"].ToString());
            DateTime dt = DateTime.Parse(vehicleActivity.ActivityDateTime);

            // Convert to PST.
            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Pacific SA Standard Time");
            DateTime pstDt = TimeZoneInfo.ConvertTime(dt, timeZoneInfo);

            PositionModel position = new PositionModel();
            position.DateAndTime = pstDt.ToUniversalTime().ToString();
            position.Longitude = vehicleActivity.Longitude;
            position.Latitude = vehicleActivity.Latitude;
            position.Speed = vehicleActivity.Speed;
            position.Heading = vehicleActivity.HDG;

            ViewBag.StyleDiv = "";
            ViewBag.BtnDiv = "display:none";
            return View(position);
        }

        // AjaxCall
        public IActionResult AjaxCall()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}