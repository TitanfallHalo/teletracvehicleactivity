using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using TeletracNavmanSiteKH.Controllers;

namespace Tests
{
    /// <summary>
    ///  Tests.
    /// </summary>
    public class Tests
    {
        /// <summary>
        ///  Test : Teletrac controller call.
        /// </summary>
        [Test]
        public void TestTeletrac()
        {
            ITempDataProvider tempDataProvider = Mock.Of<ITempDataProvider>();
            TempDataDictionaryFactory tempDataDictionaryFactory = new TempDataDictionaryFactory(tempDataProvider);
            ITempDataDictionary tempData = tempDataDictionaryFactory.GetTempData(new DefaultHttpContext());

            var obj = new HomeController()
            {
                TempData = tempData
            };

            var actResult = obj.Teletrac() as ViewResult;
            Assert.That(actResult.TempData, Is.Not.Null);
            Assert.That(actResult.TempData.Keys.Count, Is.EqualTo(1));
        }

        /// <summary>
        ///  Test : Teletrac API Call.
        /// </summary>
        [Test]
        public void TestTeletracAJAX()
        {
            var objAPI = new TeletracAPIController();
            var response = objAPI.Get();
            Assert.IsInstanceOf<OkObjectResult>(response);
        }
    }
}